package modele;

public class Employe {
  private String NomE ;
  private String Adresse ;
  
   public String getNomE() {
    return NomE;
  }
  
  public void setNomE(String n) {
    NomE = n;
  }
 
  public String getAdresse() {
    return Adresse;
  }
  
  public void setAdresse(String a) {
    Adresse = a;
  }
  
}
